﻿/*
The following file, provides the Event Class and its components.
It is used to create local Events on the Browser for easier modification.

*/
function Event(EventID) {
    //generic variables 
    var _id = EventID,
        _name = null,
        _privacy = null,
        _latitude = null,
        _longitude = null,
        _datestamp = null,
        _datecreated = null,
        _eventTypeid = -2,
        _address = null,
        _postcode = null,
        _description = null,
        _upvote = null,
        _downvote = null,
        _Venue = true,
        _Weather = false,
        _Traffic = false,
        _OfImportance = false,
        _marker = null,
        _publisher = null,
        _typeid = null,
        _marketed = false,
        _marker = null,
        _categoryid = null;

    //image
    var iconimage = '../images/Pins/StarBlue.png';

    //weather variables
    var _temperature = null,
        _windP = null,
        _windD = null,
        _forecast = null;

    //traffic variables
    //empty atm

    //venue variables
    //empty atm

    //OfImportance variables
    var _important = false;

    //test function
    this.IsEvent = function () { return true; }

    //Getters
    this.getEventID = function () { return _id; }
    this.getThisEventID = function () { return this._id; }
    this.getEventName = function () { return this._name; }
    this.getPrivacyInt = function () { return this._privacy; }
    this.getLat = function () { return this._latitude; }
    this.getLon = function () { return this._longitude; }
    this.getEventDate = function () { return this._datestamp; }
    this.getTypeInt = function () { return this._eventTypeid; }
    this.getAddress = function () { return this._address; }
    this.getPostCode = function () { return this._postcode; }
    this.getDescription = function () { return this._description; }
    this.getUpVoteInt = function () { return this._upvote; }
    this.getDownVoteInt = function () { return this._downvote; }
    this.Weather = function () { return this._Weather; }
    this.Traffic = function () { return this._Traffic; }
    this.OfImportance = function () { return this._OfImportance; }
    this.Venue = function () { return this._Venue; }
    this.getMarker = function () { return this._Marker; }
    this.getPublisher = function () { return this._publisher; }
    this.getCategoryId = function () { return this._categoryid; }

    //Specific Getters
    //Weather
    this.getWindP = function () { if (this._Weather) { return this._windP; } }
    this.getWindD = function () { if (this._Weather) { return this._windD; } }
    this.getWindTemp = function () { if (this._Weather) { return this._temperature; } }
    this.getWindForecast = function () { if (this._Weather) { return this._forecast; } }

    //Traffic
    //empty

    //Venue
    //empty

    //OfImportance
    this.getImportance = function () { if (this._OfImportance) { return this._important; } }

    //Setters
    this.setEventID = function (nValue) { this._id = nValue; }
    this.setEventName = function (nValue) { this._name = nValue; }
    this.setPrivacyInt = function (nValue) { this._privacy = nValue; }
    this.setLat = function (nValue) { this._latitude = nValue; }
    this.setLon = function (nValue) { this._longitude = nValue; }
    this.setEventDate = function (nValue) { this._datestamp = nValue; }

    this.setAddress = function (nValue) { this._address = nValue; }
    this.setPostCode = function (nValue) { this._postcode = nValue; }
    this.setDrescription = function (nValue) { this._description = nValue; }
    this.setUpVote = function (nValue) { this._upvote = nValue; }
    this.setDownVote = function (nValue) { this._downvote = nValue; }
    this.setVenue = function (nValue) { this._Venue = nValue; this._Traffic = this._Weather = this._OfImportance != nValue }
    this.setMarketed = function (nValue) { this._marketed = nValue; }
    this.setPublisherID = function (nValue) { this._publisher = nValue; }
    this.setCategoryID = function (nValue) { this._categoryid = nValue; }

    this.setWeather = function (Temperature, WindPower, WindDirection, Forecast) {
        this._Weather = true;
        this._windP = WindPower;
        this._windD = WindDirection;
        this._temperature = Temperature;
        _Traffic = _Venue = _OfImportance != false
        _Weather = true;
    }

    this.setTraffic = function (nValue) { _Traffic = nValue; _Venue = _Weather = _OfImportance != nValue }

    this.setOfImportance = function (nValue) { _OfImportance = nValue; _Traffic = _Weather = _Venue != nValue }

    this.setMarker = function (nValue) { _Marker = nValue; }

    this.getIcon = function () {
        if (iconimage != null || iconimage === undefined) {
            arrayEventType.forEach(function (k) {
                if (k.id == _eventTypeid && k.name === "venue") { iconimage = '../images/Pins/bluestar.png' }
                else if (k.id == _eventTypeid && k.name === "traffic") { iconimage = '../images/Pins/StarBlue.png' }
                else if (k.id == _eventTypeid && k.name === "weather") { iconimage = '../images/Pins/StarBlue.png' }
                else if (k.id == _eventTypeid && k.name === "ofimportance") { iconimage = '../images/Pins/StarBlue.png' }
            });
        }
        else { iconimage = '../images/Pins/StarBlue.png' }
        return iconimage;
    }

    this.setTypeInt = function (nValue) {
        _eventTypeid = nValue;
    }


    //Function that retuns the values of the Event in a JSON string Format.
    this.toJSONString = function () {
        if (this._Venue) {
            return {
                ID: _id, EventName: this._name, TypeID: this._eventTypeid, Marketed: this._marketed, Lat: this._latitude, Lon: this._longitude, Privacy: this._privacy,
                DateCreated: this._datecreated, DateStamp: this._datestamp, Address: this._address, PostCode: this._postcode, Up: this._upvote,
                Down: this._downvote, Desc: this._description, Publisher: this._publisher, CategoryID: this._categoryid
            }
        }
        else if (this._Weather) {
            return {
                ID: _id, TypeID: this._eventTypeid, EventName: this._name, Marketed: this._marketed, Lat: this._latitude, Lon: this._longitude, Privacy: this._privacy,
                DateCreated: this._datecreated, DateStamp: this._datestamp, Address: this._address, PostCode: this._postcode, Up: this._upvote,
                Down: this._downvote, Desc: this._description, Publisher: this._publisher, WindP: this._windP, WindD: this._windD,
                Temperature: this._temperature, Forecast: this._forecast
            }

        }
        else if (this._Traffic) {
            return {
                ID: _id, EventName: this._name, TypeID: this._eventTypeid, Marketed: this._marketed, Lat: this._latitude, Lon: this._longitude, Privacy: this._privacy,
                DateCreated: this._datecreated, DateStamp: this._datestamp, Address: this._address, PostCode: this._postcode, Up: this._upvote,
                Down: this._downvote, Desc: this._description, Publisher: this._publisher
            }
        }
        else if (this._OfImportance) {
            return {
                ID: _id, EventName: this._name, TypeID: this._eventTypeid, Marketed: this._marketed, Lat: this._latitude, Lon: this._longitude, Privacy: this._privacy,
                DateCreated: this._datecreated, DateStamp: this._datestamp, Address: this._address, PostCode: this._postcode, Up: this._upvote,
                Down: this._downvote, Desc: this._description, Publisher: this._publisher, CategoryID: this._categoryid
            }
        }
        else {
            return {
                ID: _id, EventName: this._name, TypeID: this._eventTypeid, Marketed: this._marketed, Lat: this._latitude, Lon: this._longitude, Privacy: this._privacy,
                DateCreated: this._datecreated, DateStamp: this._datestamp, Address: this._address, PostCode: this._postcode, Up: this._upvote,
                Down: this._downvote, Desc: this._description, Publisher: this._publisher, CategoryID: this._categoryid
            }
        }

    }
}

// Ajax call to populate the Event with the stored details based on the EventID
function EventGetData(e) {
    var success = false;
    $.ajax({
        type: "POST",
        url: window.location.origin + '/getEventDetails',
        contentType: 'application/json',
        dataType: 'json',
        async: true,
        data: JSON.stringify({ id: e.getEventID() }),
        success: function (result) {
            var result = result[0];
            e.setEventName(result.EventName);
            e.setPrivacyInt(result.PrivacyLevel);
            e.setTypeInt(result.TypeInt);
            e.setAddress(result.Address);
            e.setPostCode(result.PostCode);
            e.setDrescription(result.Desc);
            e.setDownVote(result.Down);
            e.setUpVote(result.Up);
            e.setEventDate(result.ExpectedDate);
            e.setLat(result.Lat);
            e.setLon(result.Lon);
            e.setMarketed(result.Marketed);
            e.setCategoryID(result.setCategoryID);
            e.setPublisherID(result.setPublisherID);

            success = true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            success = false;
        }
    });
    return success;
}

//Ajax post of Event Data in order to be stored in the DB
//TODO: At the moment the function will send the Event in full regardless if its new or old, a further implementation will distinquish the two and upload only the neccesary details.

function Save(oldEvent, newEvent) {
    var success = false;
    if (oldEvent == null) { // IF old is nothing then create a new one!
        $.ajax({
            type: "POST",
            url: window.location.origin + '/registerEvent',
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            data: JSON.stringify(newEvent.toJSONString()),
            success: function (result) {
                abc = result;
                newEvent.setEventID(abc.eventid.create_event);
                success = true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error ' + textStatus + " " + errorThrown);
                success = false;
            }
        });
    }
    else {
        $.ajax({
            type: "POST",
            url: window.location.origin + '/updateEvent',
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            data: JSON.stringify(newEvent.toJSONString()),
            success: function (result) {
                success = true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error ' + textStatus + " " + errorThrown);
                success = false;
            }
        });
    }
    return success;
}
