﻿//===============================================================================
//=================== SETUP =====================================================
//===============================================================================
//TODO: Finish Tweet Pin creation.

//Initialization method for map and Event creation
function init() {
    //Generic Variables
    var lastEventID = 0;
    infowindowZindex = 1;
    lastclickmilisecs = 0;
    tweetID = 0;

    arrayEvent = new Array();

    //storage of Html file into a variable for later use. 
    //NOTE: the variable is write-once-only and cannot be altered.

    $.ajax({
        type: 'GET',
        url: 'html/tweet.html',
        async: false,
        success: function (file_html) {
            Object.defineProperties(window, {
                "tweetBody": { value: file_html, writable: false }
            });
        }
    });

    //storage of Html file into a variable for later use. 
    //NOTE: the variable is write-once-only and cannot be altered.

    $.ajax({
        type: 'GET',
        url: 'html/existentEvent.html',
        async: false,
        success: function (file_html) {
            Object.defineProperties(window, {
                "existentEventHtml": { value: file_html, writable: false }
            });
        }
    });

    //storage of Html file into a variable for later use. 
    //NOTE: the variable is write-once-only and cannot be altered.

    $.ajax({
        type: 'GET',
        url: 'html/event.html',
        async: false,
        success: function (file_html) {
            Object.defineProperties(window, {
                "eventHTML": { value: file_html, writable: false }
            });
        }
    });

    //Addition of functionality not supported by IE
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }

    //Setup of Google Mapss
    var mapOptions = {
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        minZoom: 3,
        maxZoom: 23,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.BIG,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        }

    };

    var map = new google.maps.Map(document.getElementById("GoogleMap"), mapOptions);
    getGeolocationUser(map);

    adjustInfoWinodw();
    CategoryDetails()
    EventTypeDetails();

    $(document).dblclick(function (event) {
        if ($(event.target).attr('class') !== undefined) {
            if ($(event.target).attr('class').indexOf("transformable") !== -1)
            { ReplaceByInput($(event.target)); }
        }
    });

    this.addNewPin = function (e) {
        var gmapCords = new google.maps.LatLng(e.latLng.k, e.latLng.A)
        map.setCenter(gmapCords);
        lastEventID++
        var newID = lastEventID;

        var newEvent = new Event(-1);
        var contentString = NewEventBoxHTML(newID, newEvent);

        infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxHeight: 500
        });

        var marker = new google.maps.Marker({
            map: map,
            draggable: false,
            position: gmapCords,
            icon: '../images/Pins/blueq.png'
        });

        google.maps.event.addListener(marker, 'click', function () {
            if (new Date().getTime() - lastclickmilisecs < 1000) {
                lastclickmilisecs = new Date().getTime();
                $('.slowDown').show();
                setTimeout(function () { $('.slowDown').hide(); }, 500);
            }
            else {
                var goon = true;
                lastclickmilisecs = new Date().getTime();
                marker.setAnimation(google.maps.Animation.BOUNCE);

                if (newEvent.getTypeInt() == -2) {
                    $('.trafficChoiceBtn' + newID).addClass("attention");
                    $('.venueChoiceBtn' + newID).addClass("attention");
                    goon = false;
                }

                if ($("#EventName" + newID).val().length <= 0)
                { $("#EventName" + newID).addClass("attention"); goon = false; }

                if ($("#EventDate" + newID).val().length <= 0)
                { $("#EventDate" + newID).addClass("attention"); goon = false; }

                if ($("#EventDesc" + newID).val().length <= 0)
                { $("#EventDesc" + newID).addClass("attention"); goon = false; }
                if (goon) {

                    newEvent.setEventName($("#EventName" + newID).val());
                    newEvent.setEventDate($("#EventDate" + newID).val());
                    newEvent.setDrescription($("#EventDesc" + newID).val());
                    newEvent.setPrivacyInt(1);
                    newEvent.setLat(gmapCords.lat());
                    newEvent.setLon(gmapCords.lng());
                    newEvent.setMarketed(false);
                    newEvent.setPublisherID(16384);

                    //If the event is succesfuly saved, then we add a pin for an existing event and stop the animation of the pin.
                    if (Save(null, newEvent)) {
                        addExistringPin(newEvent);
                        marker.setAnimation(null); infowindow.close(); marker.setMap(null);
                    }
                }
            }
        });
        setTimeout(function () { infowindow.open(map, marker, newID); }, 100);

        google.maps.event.addListener(infowindow, 'domready', function () {
            $('#EventDate' + newID).on("click", function () { $('#EventDate' + newID).removeClass("attention"); })
            $('#EventName' + newID).on("click", function () { $('#EventName' + newID).removeClass("attention"); })
            $('#EventDesc' + newID).on("click", function () { $('#EventDesc' + newID).removeClass("attention"); })
            $('#EventHour' + newID).change(function () {
                if (re.test($(this).val())) {
                    $(this).addClass("attention");
                    $(this).val("");
                }
                else {
                    $(this).removeClass("attention");
                }
            });
            $('#EventMin' + newID).change(function () {
                if (re.test($(this).val())) {
                    $(this).addClass("attention");
                    $(this).val("");
                }
                else {
                    $(this).removeClass("attention");
                }
            });
        });

        //Register for Event on InfoWindow Open!
        $(document).on("InfoWinodwOpen" + newID, function () {
            new google.maps.Geocoder().geocode({ 'latLng': gmapCords }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address' + newID).val(results[0].formatted_address);
                        newEvent.setAddress(results[0].formatted_address);
                    }
                } else {
                    alert("Geocoder failed due to: " + status);
                }
            });
        });

        //Event listeners for Category options
        $(document).on("VenueClick" + newID, function () {
            arrayEventType.forEach(function (k) { if (k.name === "venue") { newEvent.setTypeInt(k.id); } });
            newEvent.setVenue(true);
            marker.setIcon(newEvent.getIcon());
            $('.venueChoiceBtn' + newID).removeClass("attention"); $('.trafficChoiceBtn' + newID).removeClass("attention");
        });

        $(document).on("TrafficClick" + newID, function () {
            arrayEventType.forEach(function (k) { if (k.name === "traffic") { newEvent.setTypeInt(k.id); } });
            newEvent.setTraffic(true);
            marker.setIcon(newEvent.getIcon());
            $('.trafficChoiceBtn' + newID).removeClass("attention"); $('.venueChoiceBtn' + newID).removeClass("attention");
        });

        $(document).on("catListChange" + newID, function () {
            newEvent.setCategoryID($('#categoryList' + newID + ' option:selected').val());
        });

    };

    google.maps.event.addListener(map, 'rightclick', function (e) {
        addNewPin(e);
    });

    this.initExistingEvent = function (e) {
        if (e.getThisEventID() != -1) {
            EventGetData(e);
        }
    };
    this.addExistringPin = function (Event) {
        //custom variable for gmaps infowindow
        google.maps.InfoWindow.prototype.toOpen = true;

        //initialization of the infowindow 
        var infowindow = new google.maps.InfoWindow({
            //the content if being generated by the following Function
            content: ExistingEventBoxHTML(Event),
            maxWidth: 500,
            height: 200
        });

        var marker = new google.maps.Marker({
            map: map,
            draggable: false,
            position: new google.maps.LatLng(Event.getLat(), Event.getLon()),
            icon: Event.getIcon()
        });

        google.maps.event.addListener(marker, 'click', function () {
            if (new Date().getTime() - lastclickmilisecs < 1000) {
                lastclickmilisecs = new Date().getTime();
                $('.slowDown').show();
                setTimeout(function () { $('.slowDown').hide(); }, 500);
            }
            else {
                lastclickmilisecs = new Date().getTime();
                if (infowindow.toOpen) {

                    infowindow.setZIndex(0);
                    infowindow.open(map, marker);
                    infowindow.toOpen = false;
                }

                else if (!infowindow.toOpen && infowindow.getZIndex() != 0 && infowindow.getZIndex() < infowindowZindex) {
                    infowindow.setZIndex(infowindowZindex);
                    infowindow.toOpen = false;
                }
                else if (!infowindow.toOpen) {

                    var newEvent = Event // add code for gathering data from infowindow!
                    newEvent.setEventName($("#EventName" + Event.getEventID()).html());
                    newEvent.setEventDate($("#EventDate" + Event.getEventID()).html());
                    newEvent.setDrescription($("#EventDesc" + Event.getEventID()).html());
                    newEvent.setPrivacyInt(Event.getPrivacyInt());
                    newEvent.setTypeInt(Event.getTypeInt());
                    newEvent.setLat(Event.getLat());
                    newEvent.setLon(Event.getLon());
                    newEvent.setPostCode(Event.getPostCode());
                    newEvent.setAddress(Event.getAddress());
                    newEvent.setMarketed(false);
                    newEvent.setPublisherID(Event.getPublisher());

                    if (Save(Event, newEvent)) {
                        infowindow.setContent(ExistingEventBoxHTML(newEvent));
                        infowindow.close();
                        infowindow.toOpen = true;
                    }
                }
            }
        });
    };
    google.maps.event.addListenerOnce(map, 'idle', function () { RequestData(); TweetTwitter(); });

    this.addTweetPin = function (tweet) {
        if (tweet.cords != undefined && tweet.location != undefined) {
            var gmapCords = null;

            if (tweet.cords !== 'undefined' && tweet.cords != null) {
                gmapCords = tweet.cords;
            }
            else if(tweet.location.length > 0)
            {
                new google.maps.Geocoder().geocode({ 'address': tweet.location }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        newAddress = results[0].geometry.location;
                        gmapCords = new google.maps.LatLng(parseFloat(newAddress.lat()), parseFloat(newAddress.lng()));
                    }
                });
            }

            if (gmapCords != null) {
                var infowindow = new google.maps.InfoWindow({
                    content: tweet.name.toString(),
                    maxWidth: 500,
                    height: 200
                });

                var marker = new google.maps.Marker({
                    map: map,
                    draggable: false,
                    position: gmapCords,
                    icon: '../images/Pins/blueq.png'
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
            }
            else {
                $('#spammer').append(TweetBox(tweet, tweetID));
                $("#spammer").animate({
                scrollTop: $('#spammer').get(0).scrollHeight});
                tweetID++;

            }
        }
        else
            console.log('err');
    }

    this.EventGetData = function (e) {
        var success = false;
        $.ajax({
            type: "POST",
            url: window.location.origin + '/getEventDetails',
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            data: JSON.stringify({ id: e.getEventID() }),
            success: function (result) {
                var result = result[0];
                e.setEventName(result.EventName);
                e.setPrivacyInt(result.PrivacyLevel);
                e.setTypeInt(result.TypeInt);
                e.setAddress(result.Address);
                e.setPostCode(result.PostCode);
                e.setDrescription(result.Desc);
                e.setDownVote(result.Down);
                e.setUpVote(result.Up);
                e.setEventDate(result.ExpectedDate);
                e.setLat(result.Lat);
                e.setLon(result.Lon);
                e.setMarketed(result.Marketed);
                e.setCategoryID(result.setCategoryID);
                e.setPublisherID(result.setPublisherID);

                if (e.getEventID() > lastEventID)
                { lastEventID = e.getThisEventID(); }
                addExistringPin(e);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error ' + textStatus + " " + errorThrown);
                success = false;
            }
        });
    };
}

function RequestData() {
    $.ajax({
        url: window.location.origin + '/events',
        contentType: 'application/json: charset=utf-8',
        dataType: 'json',
        type: 'GET',
        cache: false,
        timeout: 5000,
        async: true,
        success: function (result) {
            result.data.forEach(function (id) { initExistingEvent(new Event(id)); });
            return true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            return false;
        }
    });
}

function TweetBox(tweet) {
    var tempHtml = tweetBody;

    tempHtml = tempHtml.replace('WHO', tweet.name);
    tempHtml = tempHtml.replace('WHAT', tweet.text);
    tempHtml = tempHtml.replace('RANDOM', tweetID);
    
    return tempHtml;
}

//Based on an HTML file, the body is being populated with details that will appear on the infowindow
function ExistingEventBoxHTML(Event) {
    var tempHtml = existentEventHtml;
    tempHtml = tempHtml
                     .replace('VenueBtnNoID', 'VenueBtn' + Event.getEventID().toString())
                     .replace('TrafficBtnNoID', 'TrafficBtn' + Event.getEventID().toString())
                     .replace('EventID', Event.getEventID().toString())
                     .replace('EventNameNOID', 'EventName' + Event.getEventID().toString())
                     .replace('EventDateNOID', 'EventDate' + Event.getEventID().toString())
                     .replace('EventDescNOID', 'EventDesc' + Event.getEventID().toString())
                     .replace('EventAddNOID', 'EventAdd' + Event.getEventID().toString())
                     .replace('EventNameVal', Event.getEventName().toString())
                     .replace('EventDateVal', Event.getEventDate().toString())
                     .replace('EventDescVal', Event.getDescription().toString())
                     .replace('EventAddVal', Event.getAddress().toString())
                     .replace('PrivBtnNOID', 'PrivBtn' + Event.getEventID().toString())
                     .replace('PublicBtnNOID', 'PublicBtn' + Event.getEventID().toString());

    return tempHtml;
}

//Based on an HTML file, the body is being populated with details that will appear on the infowindow
function NewEventBoxHTML(newID) {
    var tempEventHTML = eventHTML;

    var tempEventHTML = tempEventHTML
                .replace('venueChoiceBtnNOID', 'venueChoiceBtn' + newID.toString())
                .replace('trafficChoiceBtnNOID', 'trafficChoiceBtn' + newID.toString())
                .replace('l1NewID', 'l1' + newID.toString())
                .replace('l2NewID', 'l2' + newID.toString())
                .replace('addressNOID', 'address' + newID.toString())
                .replace('EventNameNOID', 'EventName' + newID.toString())
                .replace('EventDateNOID', 'EventDate' + newID.toString())
                .replace('EventDescNOID', 'EventDesc' + newID.toString())
                .replace('TrafficClickNOID', 'TrafficClick' + newID.toString());

    return tempEventHTML;
}

//Generic trigger Event function
function triggerEv(eventName) {
    $.event.trigger({
        type: eventName
    });
}

//Geolocation lookup for user
function getGeolocationUser(map) {
    if (navigator.geolocation) {
        browserSupportFlag = true;
        navigator.geolocation.getCurrentPosition(function (position) {
            initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(initialLocation);
        }, function () {
            handleNoGeolocation(browserSupportFlag);
        });
    }
    // Browser doesn't support Geolocation
    else {
        browserSupportFlag = false;
        handleNoGeolocation(browserSupportFlag);
    }

    function handleNoGeolocation(errorFlag) {
        if (errorFlag == true) {
            alert("Geolocation service failed.");
        } else {
            alert("Your browser doesn't support geolocation.");
        }
        map.setCenter(new google.maps.LatLng('46.437857', '-113.466797'));
    }
}

//Population of the Category details from DB
function CategoryDetails() {
    var success = false;
    $.ajax({
        type: "GET",
        url: window.location.origin + '/categoryDetails',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (result) {

            Object.defineProperties(window, {
                "arrayCategoryOptions": { value: result.data, writable: false }
            });
            success = true;

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            success = false;
        }
    });
    return success;
}

//Population of the Event Type details from DB
function EventTypeDetails() {
    var success = false;
    $.ajax({
        type: "GET",
        url: window.location.origin + '/eventtypeDetails',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (result) {
            success = true;
            Object.defineProperties(window, {
                "arrayEventType": { value: result.data, writable: false }
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            success = false;
        }
    });
    return success;
}

//Custom additions to the Info Window
function adjustInfoWinodw() {
    google.maps.InfoWindow.prototype._open = google.maps.InfoWindow.prototype.open;
    google.maps.InfoWindow.prototype._close = google.maps.InfoWindow.prototype.close;
    google.maps.InfoWindow.prototype._openedState = false;

    google.maps.InfoWindow.prototype.open =
    function (map, anchor, ID) {
        this._open(map, anchor);
        $.event.trigger({
            type: "InfoWinodwOpen" + ID,
        });
        this._openedState = true;
    };

    google.maps.InfoWindow.prototype.close =
        function (ID) {
            this._close();
            this._openedState = false;
        };

    google.maps.InfoWindow.prototype.getOpenedState =
        function () {
            return this._openedState;
        };

    google.maps.InfoWindow.prototype.setOpenedState =
        function (val) {
            this._openedState = val;
        };
}

//Function to replace labels with textboxes for input on double click
function ReplaceByInput(clickedElement) {
    var clickedElementValue = null;
    if ($(clickedElement).val() != "" && $(clickedElement).val() != undefined) {
        clickedElementValue = $(clickedElement).val();
    }
    else if ($(clickedElement).html() != "" && $(clickedElement).html() != undefined) {
        clickedElementValue = $(clickedElement).html();
    }

    // store the new input element
    var input = $('<input type="text"></input>').addClass("replaceInput");
    $(input).val(clickedElementValue);
    $(clickedElement).replaceWith(input);

    // set cursor and focus to input element
    input[0].selectionStart = input[0].selectionEnd = input.val().length;
    $(input).focus();

    var rereplace = function () {
        $(input).on("blur", function () {
            $(clickedElement).html($(input).val());
            $(input).replaceWith(clickedElement);
        });
    }

    //safety net!
    setTimeout(rereplace, 100);
}

//Setup of the Tweet Stream and Web Socket
//The websocket requires the Twitter token for the user, we need to grab that using the Ajax and then pass it to the web socket as a variable.

function TweetTwitter() {
    $.ajax({
        url: window.location.origin + '/getTokenDetails',
        contentType: 'application/json: charset=utf-8',
        dataType: 'json',
        type: 'GET',
        cache: false,
        async: true,
        success: function (result) {
            try {
                 //= io.connect();
                var socket = io.connect(window.location.origin.toString(), { 'force new connection': true });
                socket.on('connect', function () {
                    if (result.msg === undefined) { 
                        socket.emit('requestStream', result);
                    }
                });
                socket.on('eventDetails', function (data) {

                });
                socket.on('tweet', function (data) {
                    addTweetPin(data)
                });
                socket.on('disconnect', function () {
                });
            }
            catch (err)
            { console.log(err); }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            return false;
        }
    });
}