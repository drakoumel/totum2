﻿
function init() {
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
    console.log("test");
    $.ajax({
        url: window.location.origin + '/blog/getEntries',
        contentType: 'application/json: charset=utf-8',
        dataType: 'json',
        type: 'GET',
        cache: false,
        timeout: 5000,
        async: false,
        success: function (result) {
            computeData(result.data);
            console.log("ok");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
        }
    });
}

function computeData(dataset)
{
    var wrapper = $('.entryContainer');
    var entryDiv = $('<div></div>').attr("class","w-container entry");
    dataset.forEach(function (entry) {
        entryDiv.append($('<h1></h1>').html(entry.title));
        entryDiv.append($('<p></p>').html(entry.content));
        entryDiv.append($('<br />'));
        entryDiv.append($('<br />'));
        console.log(entryDiv);
        wrapper.append(entryDiv);
    });

    
}
