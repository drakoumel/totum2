module.exports = function (app, passport) {

    // normal routes ===============================================================
    // the file bellow, dictates which pages/functions will operate based on the request url of the user's client

    // show the home page (will also have our login links)
    // MAIN INDEX ============================
    app.get('/', isLoggedIn, function (req, res) {
        res.render('totum.ejs', {
            user: req.user
        });
    });

    app.get('/control', function (req, res) {
        res.render('index.ejs');
    });

    // blog
    app.get('/blog', function (req, res) {
        res.render('blog.ejs');
    })

    app.get('/blog/getEntries', require('../app/functions/blog').getData);

    // PROFILE SECTION =========================
    app.get('/profile', isLoggedIn, function (req, res) {
        res.render('profile.ejs', {
            user: req.user
        });
    });

    // LOGOUT ==============================
    app.get('/logout', isLoggedIn, function (req, res) {
        req.logout();
        res.redirect('/');
    });

    // =============================================================================
    // AUTHENTICATE (FIRST LOGIN) ==================================================
    // =============================================================================

    // locally --------------------------------
    // LOGIN ===============================
    // show the login form
    app.get('/login', function (req, res) {
        res.render('login.ejs', { message: req.flash('loginMessage') });
    });

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    // SIGNUP =================================
    // show the signup form
    app.get('/signup', function (req, res) {
        res.render('signup.ejs', { message: req.flash('loginMessage') });
    });

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/', // redirect to the secure profile section
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    // facebook -------------------------------

    app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

    // callback on authorized user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/',
            failureRedirect: '/control'
        }));

    // twitter --------------------------------

    app.get('/auth/twitter', passport.authenticate('twitter', { scope: 'email' }));

    // callback on authorized user
    app.get('/auth/twitter/callback',
        passport.authenticate('twitter', {
            successRedirect: '/',
            failureRedirect: '/control'
        }));


    // google ---------------------------------

    app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

    // callback on authorized user
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/profile',
            failureRedirect: '/control'
        }));

    // =============================================================================
    // AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
    // =============================================================================

    // locally --------------------------------
    app.get('/connect/local', function (req, res) {
        res.render('connect-local.ejs', { message: req.flash('loginMessage') });
    });
    app.post('/connect/local', passport.authenticate('local-signup', {
        successRedirect: '/profile', // redirect to the secure profile section
        failureRedirect: '/connect/local', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    // facebook -------------------------------

    app.get('/connect/facebook', passport.authorize('facebook', { scope: 'email' }));

    // callback on authorized user
    app.get('/connect/facebook/callback',
        passport.authorize('facebook', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    // twitter --------------------------------

    app.get('/connect/twitter', passport.authorize('twitter', { scope: 'email' }));

    // callback on authorized user
    app.get('/connect/twitter/callback',
        passport.authorize('twitter', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));


    // google ---------------------------------

    app.get('/connect/google', passport.authorize('google', { scope: ['profile', 'email'] }));

    // callback on authorized user
    app.get('/connect/google/callback',
        passport.authorize('google', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    // =============================================================================
    // UNLINK ACCOUNTS =============================================================
    // =============================================================================
    // Remove token details in order to unlick accounts or email/password for local
    // user account will stay active in case they want to reconnect in the future

    // local -----------------------------------
    app.get('/unlink/local', function (req, res) {
        var user = req.user;
        user.local.email = undefined;
        user.local.password = undefined;
        user.save(function (err) {
            res.redirect('/profile');
        });
    });

    // facebook -------------------------------
    app.get('/unlink/facebook', function (req, res) {
        var user = req.user;
        user.facebook.token = undefined;
        user.save(function (err) {
            res.redirect('/profile');
        });
    });

    // twitter --------------------------------
    app.get('/unlink/twitter', function (req, res) {
        var user = req.user;
        user.twitter.token = undefined;
        user.save(function (err) {
            res.redirect('/profile');
        });
    });

    // google ---------------------------------
    app.get('/unlink/google', function (req, res) {
        var user = req.user;
        user.google.token = undefined;
        user.save(function (err) {
            res.redirect('/profile');
        });
    });

    // =============================================================================
    // FUNCTIONS ===================================================================
    // =============================================================================

    // Routes used by Ajax calls and require the webserv module.
    app.get('/categoryDetails', isLoggedIn, require('./modules/webserv').categoryDetails);
    app.get('/eventtypeDetails', isLoggedIn, require('./modules/webserv').eventtypeDetails);
    app.get('/events', isLoggedIn, require('./modules/webserv').Events);
    app.post('/getEventDetails', isLoggedIn, require('./modules/webserv').eventDetails);
    app.post('/registerEvent', isLoggedIn, require('./modules/webserv').registerEvent);
    app.post('/updateEvent', isLoggedIn, require('./modules/webserv').updateEvent);
    app.get('/getTokenDetails', isLoggedIn, require('./modules/webserv').getTokenDetails);
};



// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/control');
}