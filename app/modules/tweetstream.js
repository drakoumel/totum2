//modules/tweetstream.js

module.exports = function (server) {

    //required modules
    var io = require('socket.io').listen(server);
    var _ = require('underscore');
    var twitter = require('ntwitter');
    var request = require("request");
    var pg = require('pg');

    // Load the file that contain the keys for Authentication with Third party applications (facebook,twitter etc)
    var configAuth = require('./../../config/auth'); 

    //The socket is opened
    io.sockets.on('connection', function (client) {
        //on receival of the message "requestStream" the following code is executed
        client.on('requestStream', function (msg) {

            //Set up of the User's auth token details based on the message received
            //The oauth needs to be stored for later use, as although we are being Authenticated through the twit client the request, requires again the oauth details
           var oauth =
           {
               consumer_key: configAuth.twitterAuth.consumerKey
               , consumer_secret: configAuth.twitterAuth.consumerSecret
               , token: msg.token
               , token_secret: msg.sToken
           };


            var twit = new twitter(
                {
                    consumer_key: configAuth.twitterAuth.consumerKey,           
                    consumer_secret: configAuth.twitterAuth.consumerSecret,        
                    access_token_key: msg.token,       
                    access_token_secret: msg.sToken     
                })
                   .verifyCredentials(function (err, data) {
                       if (err) {
                           console.log(err);
                       } else {

                           //GET LIST OF FRIENDS
                           var url = 'https://api.twitter.com/1.1/friends/ids.json?cursor=-1&screen_name=' + msg.username + '&count=5000';
                           var userFriendsList = null;

                           request.get({ url: url, oauth: oauth, json: true }, function (e, response, user) {
                               userFriendsList = user.ids.toString();
                               if (userFriendsList != '' && userFriendsList != null && userFriendsList != undefined && userFriendsList.length > 0) {
                                   //After the friend list is populated we start the search stream with filters based on the friends list
                                   twit.stream('statuses/filter', { follow: user.ids.toString() }, function (stream) {
                                       stream.on('data', function (tweet) {
                                           //On receival of data, we check if the tweet is actuall complete and if so emit to the connected client the data
                                           if (tweet.text !== undefined) {
                                               client.emit('tweet', tweet = { text: tweet.text, name: tweet.user.screen_name, cords: tweet.user.location.coordinates +'', created_at: tweet.created_at, location: tweet.user.location });
                                           }
                                           else
                                               console.log('undefined tweet');
                                       });
                                   });
                               }
                           });
                       }
                   });
        });

        //Experimental function.
        //Retrieve the Event details through a web socket instead of ajax call,
        // NOT SUPPORTED

        client.on('eventDetails', function (msg) {
            console.log(msg.sToken, msg.token, msg.username);
            
            var localconstring = "postgres://admin:cswow2010@37.247.55.226:5432/totum";

            var client = new pg.Client(localconstring);
            client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
            client.connect(function (err, client, done) {

                var data = [];
                prepStatement = {
                    name: 'select event details based on id',
                    text: 'SELECT * FROM event_details where id = $1',
                    values: [req.body.id]
                };
                var query = client.query(prepStatement);

                query.on('row', function (result) {

                    var dataItem = {
                        EventName: result.name, PrivacyLevel: result.privacyid, TypeInt: result.eventtypeid, Lat: result.lat,
                        Lon: result.lon, ExpectedDate: result.dateexpected, Desc: result.description, Down: result.downvote,
                        Up: result.upvote, PostCode: result.postcode, Address: result.address, Marketed: result.marketed,
                        CategoryID: result.categoryid, PublisherID: result.publisherid
                    };
                    data.push(dataItem);
                });

                query.on('error', function (err) {
                    console.log(err);
                    res.write(JSON.stringify({ error: err }));
                });

                query.on('end', function () {
                    client.emit('eventDetails', JSON.stringify(data));
                });
            });
        });

        //Test function
        client.on('message', function () {
            client.send('message');
        });

        //Function for debugging and logging purpuses
        client.on('disconnect', function () {
            console.log(client, 'disconected');
        });
    });
};


