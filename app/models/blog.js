// load the things we need
var mongoose = require('mongoose');

// define the schema for our blog model
var blogSchema = mongoose.Schema({
    title:String,
    content:String,
    datestamp: Date
});

module.exports = mongoose.model('Blog', blogSchema);