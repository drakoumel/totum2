exports.getData = function (req, res) {
        var pg = require('pg');
        var localconstring = "postgres://admin:cswow2010@37.247.55.226:5432/totum";

        var client = new pg.Client(localconstring);
        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect(function (err, client, done) {

            var data = [];
            var query = client.query('SELECT  title,content,datestamp FROM public.blog order by datestamp desc');

            query.on('row', function (result) {
                var dataItem = { title: result.title, date: result.datestamp, content: result.content };
                data.push(dataItem);
            });

            query.on('error', function (err) {
                console.log(err);
                res.write(JSON.stringify({ error: err }));
            });

            query.on('end', function () { res.end(JSON.stringify({data: data})); });
        });
};
